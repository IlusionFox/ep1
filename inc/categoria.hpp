#ifndef CATEGORIA_HPP
#define CATEGORIA_HPP
#include <bits/stdc++.h>

using namespace std;
class cat{
    public:
        cat();
        cat(string tipo);
        ~cat();
        string get_cat();
        void set_cat(string tipo);
        
        
    private:
        string tipo;
    
    
};
#endif
